package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3834552629113201265L;
	
	public void init() throws ServletException{
		System.out.println("RegisterServlet has been Initialized.");
	}
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		//Capture the Register information
		String firstName = req.getParameter("firstName");
		String lastName = req.getParameter("lastName");
		String email = req.getParameter("email");
		String phoneNo = req.getParameter("phoneNo");
		String appDiscovery = req.getParameter("appDiscovery");
		String userType = req.getParameter("userType");
		String dateOfBirth = req.getParameter("date_of_birth");
		String pdesc = req.getParameter("pdesc");
	
		
		//store the data 
		HttpSession session = req.getSession();
		
		session.setAttribute("firstName", firstName);
		session.setAttribute("lastName", lastName);
		session.setAttribute("email",email);
		session.setAttribute("phoneNo", phoneNo);
		session.setAttribute("appDiscovery",appDiscovery);
		session.setAttribute("userType", userType);
		session.setAttribute("dateOfBirth", dateOfBirth);
		session.setAttribute("pdesc", pdesc);
		
		res.sendRedirect("register.jsp");
	}
	public void destroy() {
		System.out.println("RegisterSerevlet has been Finalized.");
	}

}
