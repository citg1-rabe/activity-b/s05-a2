<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>home Page</title>
</head>
<body>

	<%
		String userType= session.getAttribute("userType").toString();
			if(userType.equals("Applicant")){
			userType = "Applicant";
		}
	
		else{
			userType = "Employer";
		}
	%>
		<h1> Welcome <%= session.getAttribute("fullname") %>!</h1>
		
	<%
		if (userType == "Applicant"){
			out.println("Welcome applicant. You may now start looking for your career opportunity.");
		}
		else {
			out.println("Welcome employer. You may start browsing applicant profiles.");
		}
	%>	
