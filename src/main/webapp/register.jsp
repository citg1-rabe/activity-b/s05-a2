<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registration Confirmation</title>
</head>
<body>
	<%
		String appDiscovery = session.getAttribute("appDiscovery").toString();
			if(appDiscovery.equals("friends")){
				appDiscovery = "Friends";
			}
			else if(appDiscovery.equals("SocialMedia")){
				appDiscovery= "Social Media";
			}
			else{
				appDiscovery= "Others";
			}
		%>
	
		<h1>Registration Confirmation</h1>
		<p>First Name: <%= session.getAttribute("firstName") %></p>
		<p>Last Name: <%= session.getAttribute("lastName") %></p>
		<p>Phone: <%= session.getAttribute("phoneNo") %></p>
		<p>Email: <%= session.getAttribute("email") %></p>
		<p>App Discovery: <%= appDiscovery %></p> 
		<p>Date of Birth: <%= session.getAttribute("dateOfBirth") %></p>
		<p>User Type: <%= session.getAttribute("userType") %></p>
		<p>Description: <%= session.getAttribute("pdesc") %></p>


		
		<!-- Submit button for registering -->
		<form action="login" method="post">
			<input type="submit">
		</form>
		
		<!-- Button to go back at index.jsp -->
		<form action="index.jsp">
			<input type="submit" value="Back">
		</form>
</body>
</html>